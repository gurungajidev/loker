# Mini Loker dengan Golang

Run loker.go

    go loker.go


Ketikkan beberapa perintah sebagai berikut :
- init [jumlah loker] : untuk membuat jumlah loker
- status : untuk menampilkan status dari masing - masing nomor loker
- input [tipe identitas] [nomor identitas] : untuk memasukkan kartu identitas
- leave [nomor loker] : untuk mengosongkan loker
- find [nomor identitas] : akan menampilkan nomor loker berdasar nomor identitas
- search [tipe identitas] : akan menampilkan daftar nomor identitas sesuai tipe
identitas yang dicari
- exit : untuk mengakhiri program