package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// pesan
var message string = "\n\n\n" +
	"init [jumlah loker] \t\t\t\t: untuk membuat jumlah loker \n" +
	"status \t\t\t\t\t\t: untuk menampilkan status dari masing - masing nomor loker \n" +
	"input [tipe identitas] [nomor identitas] \t: untuk memasukkan kartu identitas \n" +
	"leave [nomor loker] \t\t\t\t: untuk mengosongkan loker \n" +
	"find [nomor identitas] \t\t\t\t: akan menampilkan nomor loker berdasar nomor identitas \n" +
	"search [tipe identitas] \t\t\t: akan menampilkan daftar nomor identitas sesuai tipe identitas yang dicari \n" +
	"exit \t\t\t\t\t\t: untuk mengakhiri program\n\n" +
	"Ketikkan Pilihan : "

// daftar isi loker
var list_loker [][3]string

// main
func main() {
	// reader
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print(message)
		// input
		input, _ := reader.ReadString('\n')

		// split inputan
		split := strings.Split(input[:len(input)-2], " ")

		// cek menu yg dipilih
		switch menu := strings.ToLower(split[0]); menu {
		case "init":
			initLoker(split)
		case "status":
			statusLoker(split)
		case "input":
			inputLoker(split)
		case "leave":
			leaveLoker(split)
		case "find":
			findLoker(split)
		case "search":
			searchLoker(split)
		case "exit":
			os.Exit(0)
		default:
			fmt.Println("Menu yang dipilih tidak ada")
		}
	}
}

// membuat loker baru
func initLoker(input []string) {
	// cek format init
	if len(input) < 2 {
		fmt.Println("Init tidak sesuai format")
		return
	}
	// ubah inputan ke int
	cast, err := strconv.Atoi(input[1])
	if err != nil {
		fmt.Println("Ukuran loker harus angka")
		return
	}
	// init array
	list_loker = nil
	for i := 0; i < cast; i++ {
		// data awal
		new_array := [3]string{strconv.Itoa(i + 1), "-\t", "-\t"}
		// tambahkan ke list loker
		list_loker = append(list_loker, new_array)
	}
	fmt.Printf("Berhasil membuat loker dengan jumlah %d\n", len(list_loker))
}

// lihat semua isi loker
func statusLoker(input []string) {
	// cek loker
	if list_loker == nil {
		fmt.Println("Loker belum dibuat")
		// return
		return
	}
	// header
	fmt.Printf("No Loker\tTipe Identitas\tNo Identitas\n")
	// looping loker
	for i := 0; i < len(list_loker); i++ {
		fmt.Printf("%v\t\t%v\t%v\n", list_loker[i][0], list_loker[i][1], list_loker[i][2])
	}
}

// tambahkan isi loker
func inputLoker(input []string) {
	// cek loker
	if list_loker == nil {
		fmt.Println("Loker belum dibuat")
		// return
		return
	}
	// cek format input
	if len(input) < 3 {
		fmt.Println("Input tidak sesuai format")
		return
	}
	// cek jenis identitas
	if strings.ToUpper(input[1]) != "KTP" && strings.ToUpper(input[1]) != "SIM" {
		fmt.Println("Jenis identitas hanya SIM / KTP saja")
		return
	}
	// ubah inputan ke int / nomer identitas
	_, err := strconv.Atoi(input[2])
	if err != nil {
		fmt.Println("Nomor identitas harus angka")
		return
	}
	// input ke list, looping loker
	for i := 0; i < len(list_loker); i++ {
		if list_loker[i][1] == "-\t" {
			list_loker[i][1] = strings.ToUpper(input[1]) + "\t"
			list_loker[i][2] = input[2]
			/// pesan
			fmt.Printf("Kartu identitas tersimpan di loker nomor %v", (i + 1))
			// return
			return
		}
	}
	// jika tidak ada yg kosong tampilkan pesan penuh
	fmt.Println("Maaf loker sudah penuh")
}

// keluarkan isi loker
func leaveLoker(input []string) {
	// cek loker
	if list_loker == nil {
		fmt.Println("Loker belum dibuat")
		// return
		return
	}
	// cek format input
	if len(input) < 2 {
		fmt.Println("Leave tidak sesuai format")
		return
	}
	// ubah inputan ke int / nomer identitas
	cast, err := strconv.Atoi(input[1])
	if err != nil {
		fmt.Println("Nomor Loker harus angka")
		return
	}
	// cek nomor loker
	if cast < 0 || cast >= len(list_loker) {
		fmt.Println("Loker tidak ditemukan")
		return
	}
	// hapus loker sesuai index
	new_array := [3]string{input[1], "-\t", "-\t"}
	list_loker[cast-1] = new_array
	fmt.Printf("Loker nomor %v berhasil dikosongkan\n", input[1])
}

// cari nomor identitas di loker
func findLoker(input []string) {
	// cek loker
	if list_loker == nil {
		fmt.Println("Loker belum dibuat")
		// return
		return
	}
	// cek format input
	if len(input) < 2 {
		fmt.Println("Find tidak sesuai format")
		return
	}
	// cari list, looping loker
	for i := 0; i < len(list_loker); i++ {
		if strings.Contains(list_loker[i][2], input[1]) {
			/// pesan
			fmt.Printf("Kartu identitas tersebut berada di loker nomor %v", (i + 1))
			// return
			return
		}
	}
	// jika tidak ditemukan tampilkan pesan
	fmt.Println("Nomor identitas tidak ditemukan")
}

// cati isi loker sesuai identitas yang digunakan
func searchLoker(input []string) {
	// cek loker
	if list_loker == nil {
		fmt.Println("Loker belum dibuat")
		// return
		return
	}
	// cek format input
	if len(input) < 2 {
		fmt.Println("Search tidak sesuai format")
		return
	}
	// init array hasil
	var hasil []string
	// cari list, looping loker
	for i := 0; i < len(list_loker); i++ {
		if strings.Contains(list_loker[i][1], strings.ToUpper(input[1])) {
			hasil = append(hasil, list_loker[i][2])
		}
	}
	// cek hasil
	if len(hasil) > 0 {
		// jika ditemukan gabungkan
		fmt.Printf("Hasil Pencarian : %v", strings.Join(hasil[:], ",")+"\n")
	} else {
		// jika tidak ditemukan tampilkan pesan
		fmt.Println("Pencarian tidak ditemukan")
	}
}
